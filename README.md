# Issue

# Provide Inject not working when Component is imported from another project folder

This little example should show the issue with using provide and inject if the component is imported from an external project

# Use

```sh
# in ROOT FOLDER
npm install

# in src/external-folder
npm install

# in root folder
npm run serve
```

If you open up localhost:8080 in your browser now you will see that the first line is showing the injected variable **"bar"** correctly while the second line representing the component from an external folder will not display the content provided by its parent

